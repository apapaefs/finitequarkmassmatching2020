#include <memory>     // for std::unique_ptr
#include <cmath>

#include "HEJ/Analysis.hh"
#include "HEJ/Event.hh"
#include "HEJ/MatrixElement.hh"
#include "HEJ/RivetAnalysis.hh"
#include "HEJ/YAMLreader.hh"

#include "Rivet/AnalysisHandler.hh"
#include "yaml-cpp/yaml.h"

namespace {
  using namespace HEJ;
    EventTreatMap get_event_treatment(
        YAML::Node const & yaml
    ){
      using namespace event_type;
      EventTreatMap treat {
          {no_2_jets, EventTreatment::discard},
          {bad_final_state, EventTreatment::discard},
          {FKL, EventTreatment::reweight},
          {unob, EventTreatment::keep},
          {unof, EventTreatment::keep},
          {qqxexb, EventTreatment::keep},
          {qqxexf, EventTreatment::keep},
          {qqxmid, EventTreatment::keep},
          {non_resummable, EventTreatment::keep}
        };
      set_from_yaml(treat.at(FKL), yaml, "FKL");
      set_from_yaml(treat.at(unob), yaml, "unordered");
      treat.at(unof) = treat.at(unob);
      set_from_yaml(treat.at(qqxexb), yaml, "extremal qqx");
      treat.at(qqxexf) = treat.at(qqxexb);
      set_from_yaml(treat.at(qqxmid), yaml, "central qqx");
      set_from_yaml(treat.at(non_resummable), yaml, "non-resummable");
      if(treat[non_resummable] == EventTreatment::reweight){
        throw std::invalid_argument{"Cannot reweight non-resummable events"};
      }
      return treat;
    }
}


class matching_Analysis: public HEJ::Analysis {
public:
  matching_Analysis(YAML::Node const &  config, LHEF::HEPRUP const & heprup):
    rAna_(config["dummy"], heprup),
    hconfig1_{},
    hconfig2_{},
    ew_parameters_{},
    ev_treat_(get_event_treatment(config))
    {
      hconfig1_ = HEJ::get_Higgs_coupling(config, "Higgs coupling 1");
      hconfig2_ = HEJ::get_Higgs_coupling(config, "Higgs coupling 2");
      
      ew_parameters_ = HEJ::get_ew_parameters(config);

      std::cout << "h coupling 1 mt " << hconfig1_.mt << " mb " << hconfig1_.mb
        << " imf " << hconfig1_.use_impact_factors << " incb "
        << hconfig1_.include_bottom << std::endl;
      std::cout << "h coupling 2 mt " << hconfig2_.mt << " mb " << hconfig2_.mb
        << " imf " << hconfig2_.use_impact_factors << " incb "
        << hconfig2_.include_bottom << std::endl;
    }

  void fill(
    HEJ::Event const &  event ,
    HEJ::Event const &  FO_ev
  ) override {
    auto ev = event;
    if(ev_treat_.find(ev.type())->second == HEJ::EventTreatment::reweight){
      HEJ::MatrixElementConfig config;
      config.log_correction = false;
      config.ew_parameters = ew_parameters_;
      config.Higgs_coupling = hconfig1_;
      // fill alpha_s with dummy function, doesn't matter for ratio
      const HEJ::MatrixElement M1(
        [](double){ return 1.; }, config);

      config.Higgs_coupling = hconfig2_;
      const HEJ::MatrixElement M2(
        [](double){ return 1.; }, config);

      const double M1_tree_kin = M1.tree_kin(FO_ev);
      const double M2_tree_kin = M2.tree_kin(FO_ev);
      const double factor = M1_tree_kin/M2_tree_kin;

      ev.central().weight *= factor;
      for(auto & var: ev.variations())
        var.weight *= factor;
    }
    rAna_.fill(ev, FO_ev);
  }

  bool pass_cuts(
    HEJ::Event const & /* event */,
    HEJ::Event const & /* FO_event */
  ) override {
    return true;
  }

  void finalise() override {
    rAna_.finalise();
  }
private:
  HEJ::RivetAnalysis rAna_;
  HEJ::HiggsCouplingSettings hconfig1_;
  HEJ::HiggsCouplingSettings hconfig2_;
  HEJ::EventTreatMap ev_treat_;
  double vev_;
  HEJ::ParticleProperties Wprop_;
  HEJ::ParticleProperties Zprop_;
  HEJ::ParticleProperties Hprop_;
  HEJ::EWConstants ew_parameters_;
};

extern "C"
std::unique_ptr<HEJ::Analysis> make_analysis(
    YAML::Node const & config, LHEF::HEPRUP const & heprup
){
  return std::make_unique<matching_Analysis>(config, heprup);
}
