#include "HEJ/Event.hh"
/**
 * @brief returns H_T of the non decayed particles
 */
extern "C"
double HT_nodecay(HEJ::Event const & ev){
    double result = 0.;
    for(size_t i = 0; i < ev.outgoing().size(); ++i)
        result += ev.outgoing()[i].perp();
    return result;
}
