#include "HEJ/Event.hh"
/**
 * @brief returns max(m_j1j2, 125) as scale
 */
extern "C"
double MaxMjjMh(HEJ::Event const & ev){
  const auto jets = sorted_by_pt(ev.jets());
  assert(jets.size() >= 2);
  const double m12 = (jets[0] + jets[1]).m();
  return std::max(125., m12);
}
