#!/usr/bin/env python
from __future__ import with_statement
import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
import random
import sys
import time
import datetime
import os.path


parser = OptionParser(usage="%prog [hepmc file]")

opts, args = parser.parse_args()


if len(args) < 1:
    print 'usage: run_rivet_on_sherpa_hepmc.py [hepmc file]'
    exit()

ScaleArray = ["MEWeight", "MUR0.5_MUF0.5_PDF13100", "MUR0.5_MUF0.707106781187_PDF13100", "MUR0.5_MUF1_PDF13100", "MUR0.707106781187_MUF0.5_PDF13100", "MUR0.707106781187_MUF0.707106781187_PDF13100", "MUR0.707106781187_MUF1.41421356237_PDF13100", "MUR0.707106781187_MUF1_PDF13100", "MUR1.41421356237_MUF0.707106781187_PDF13100", "MUR1.41421356237_MUF1.41421356237_PDF13100", "MUR1.41421356237_MUF1_PDF13100", "MUR1.41421356237_MUF2_PDF13100", "MUR1_MUF0.5_PDF13100", "MUR1_MUF0.707106781187_PDF13100", "MUR1_MUF1.41421356237_PDF13100", "MUR1_MUF1_PDF13100", "MUR1_MUF2_PDF13100", "MUR2_MUF1.41421356237_PDF13100", "MUR2_MUF1_PDF13100", "MUR2_MUF2_PDF13100" ]

analyses = ['MC_XS', 'MC_HEJ_HJET_ADD2']
tag = 'ANALYSIS-815'

for sc in range(len(ScaleArray)):
    if sc != 0:
        current_tag = tag + '.' + ScaleArray[sc] + '.yoda'
    elif sc == 0:
        current_tag = tag + '.yoda'
    runcommand = 'export RIVET_WEIGHT_INDEX=' + str(sc) + '; rivet --analyses=' + ','.join(analyses) + ' ' + str(args[0]) + ' -o ' + current_tag
    if len(args) > 1:
        if args[1] == "test":
            runcommand = runcommand + ' -n 1000'
    print runcommand
    p = subprocess.Popen(runcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.', bufsize=1)
    for line in iter(p.stdout.readline, b''):
        print line,
    out, err = p.communicate()
    print out, err
