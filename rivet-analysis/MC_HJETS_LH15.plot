# BEGIN PLOT .*
RatioPlotSameStyle=1
RatioPlotYLabel=Ratio
XTwosidedTicks=1
YTwosidedTicks=1
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/XS
Title=Cross section
LogY=0
ShowZero=0
XLabel=
YLabel=$\sigma_\text{fid}$ [pb]
XCustomMajorTicks=0.5	 cross section
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/loose
Title=Loose cut cross sections ($\Delta\phi(h,jj)>\;$0)
XLabel=
YLabel=$\sigma_\text{loose}$ [pb]
XCustomMajorTicks=0	 dijet	1	VBF	2	VBF2
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/tight
Title=Tight cut cross sections ($\Delta\phi(h,jj)>\;$2.6)
XLabel=
YLabel=$\sigma_\text{tight}$ [pb]
XCustomMajorTicks=0	 dijet	1	VBF	2	VBF2
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/cos_theta_star
Title=Collins-Soper angle of photon pair
XLabel=$|\cos\theta^*|$
YLabel=d$\sigma$/d$|\cos\theta^*|$ [pb]
LegendXPos=0.1
LegendYPos=0.5
LogY=0
ShowZero=1
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/deltay_yy
Title=Rapidity separation of photon pair
XLabel=$\Delta y(\gamma_1,\gamma_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/m_gammagamma
Title=Invariant mass of photon pair
XLabel=$m(\gamma_1,\gamma_2)$ [GeV]
YLabel=d$\sigma$/d$m$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/pTt
Title=Transverse momentum of photon pair wrt.\ its transverse thrust axis
XLabel=$p_{\perp,t}(\gamma_1,\gamma_2)$ [GeV]
YLabel=d$\sigma$/d$p_{\perp,t}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/dR_y_j1
Title=Angular separation of leading jet and nearest photon ($N_\text{jet}\geq\;$2)
XLabel=$\Delta R(j_1,\gamma)$
YLabel=d$\sigma$/d$\Delta R$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/dR_y_j2
Title=Angular separation of subleading jet and nearest photon ($N_\text{jet}\geq\;$2)
XLabel=$\Delta R(j_2,\gamma)$
YLabel=d$\sigma$/d$\Delta R$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/NJet_incl_30
Title=Inclusive jet multiplicity ($p_\perp\geq\;$30 GeV)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/NJet_excl_30
Title=Exclusive jet multiplicity ($p_\perp\geq\;$30 GeV)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/NJet_incl_50
Title=Inclusive jet multiplicity ($p_\perp\geq\;$50 GeV)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/NJet_excl_50
Title=Exclusive jet multiplicity ($p_\perp\geq\;$50 GeV)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/NJet_incl
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
XCustomMajorTicks=0	$\geq$0	1	$\geq$1	2	$\geq$2	3	$\geq$3

# BEGIN PLOT /MC_HJETS_LH15/NJet_excl
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
XCustomMajorTicks=0	$=$0	1	$=$1	2	$=$2	3	$=$3
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_y
Title=Higgs boson rapidity
XLabel=$y(h)$
YLabel=d$\sigma$/d$y$ [pb]
LogY=0
ShowZero=1
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_pT_incl
Title=Higgs boson transverse momentum ($N_\text{jet}\geq\;$0)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_pT_excl
Title=Higgs boson transverse momentum ($N_\text{jet}=\;$0)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_j_pT_incl
Title=Higgs boson transverse momentum ($N_\text{jet}\geq\;$1)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_j_pT_excl
Title=Higgs boson transverse momentum ($N_\text{jet}=\;$1)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_jj_pT_incl
Title=Higgs boson transverse momentum ($N_\text{jet}\geq\;$2)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_jj_pT_excl
Title=Higgs boson transverse momentum ($N_\text{jet}=\;$2)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_jj_pT_VBF
Title=Higgs boson transverse momentum ($N_\text{jet}\geq\;$2)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_jjj_pT_incl
Title=Higgs boson transverse momentum ($N_\text{jet}\geq\;$3)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_jjj_pT_excl
Title=Higgs boson transverse momentum ($N_\text{jet}=\;$3)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_pT
XLabel=$p_\perp(h)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_j_pT
XLabel=$p_\perp(h)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_jj_pT
XLabel=$p_\perp(h)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_jjj_pT
XLabel=$p_\perp(h)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/Hj_pT
XLabel=$p_\perp(hj)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/Hjj_pT
XLabel=$p_\perp(hjj)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/Hj_pT_incl
Title=Higgs boson plus leading jet transverse momentum ($N_\text{jet}\geq\;$1)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/Hj_pT_excl
Title=Higgs boson plus leading jet transverse momentum ($N_\text{jet}=\;$1)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/Hjj_pT_incl
Title=Higgs boson plus two leading jets transverse momentum ($N_\text{jet}\geq\;$2)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/Hjj_pT_excl
Title=Higgs boson plus two leading jets transverse momentum ($N_\text{jet}=\;$2)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/deltaphi_Hjj_incl
Title=Azimuthal separation of Higgs boson and leading dijet system ($N_\text{jet}\geq\;$2)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/deltaphi_Hjj_excl
Title=Azimuthal separation of Higgs boson and leading dijet system ($N_\text{jet}=\;$2)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/deltaphi_Hjj
XLabel=$\Delta\phi(h,jj)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/deltay_H_jj
Title=Rapidity separation of Higgs boson and leading dijet system
XLabel=$\Delta y(h,jj)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/deltaphi_jj
Title=Azumuthal separation of the two leading jets
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/deltay_jj
Title=Rapidity separation of leading jets
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/dijet_mass
Title=Invariant dijet mass
XLabel=$m(j_1,j_2)$ [GeV]
YLabel=d$\sigma$/d$m$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/deltaphi2
Title=Angle between vector sum of jets forward and jets backward of Higgs boson
XLabel=$\Delta\phi_2$
YLabel=d$\sigma$/d$\Delta\phi_2$ [pb]
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/HT_jets
Title=Scalar sum of jet transverse momenta
XLabel=$H_T$ [GeV]
YLabel=d$\sigma$/d$H_T$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/HT_all
Title=Scalar sum of jet transverse momenta and Higgs transverse mass
XLabel=$H_T$ [GeV]
YLabel=d$\sigma$/d$H_T$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/H_dijet_mass
Title=Higgs boson plus two leading jets transverse momentum 
XLabel=$m_{hjj}$ [GeV]
YLabel=d$\sigma$/d$H_T$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet1_pT_incl
Title=Transverse momentum of leading jet ($N_\text{jet}\geq\;$1)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet1_pT_excl
Title=Transverse momentum of leading jet ($N_\text{jet}\geq\;$1)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet2_pT_incl
Title=Transverse momentum of subleading jet ($N_\text{jet}\geq\;$2)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet2_pT_excl
Title=Transverse momentum of subleading jet ($N_\text{jet}=\;$2)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet3_pT_incl
Title=Transverse momentum of 3rd jet ($N_\text{jet}\geq\;$3)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet3_pT_excl
Title=Transverse momentum of 3rd jet ($N_\text{jet}=\;$3)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet1_y
Title=Rapidity of leading jet
XLabel=$y(j_1)$
YLabel=d$\sigma$/d$y$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet2_y
Title=Rapidity of subleading jet
XLabel=$y(j_1)$
YLabel=d$\sigma$/d$y$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet3_y
Title=Rapidity of 3rd jet
XLabel=$y(j_1)$
YLabel=d$\sigma$/d$y$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet1_pT_jjpT_mindy1
Title=Transverse momentum of leading jet ($\Delta y(j_1,j_2)>\;$1)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jet1_pT_jjdy_mindy1
Title=Transverse momentum of leading jet ($\Delta y(j_\text{fw},j_\text{bw})>\;$1)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jjpT_dy
Title=Rapidity separation of leading jets
XLabel=$\Delta y (j_1,j_2)$ [GeV]
YLabel=d$\sigma$/d$\Delta y$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jjpT_dy_2j_excl
Title=Rapidity separation of leading jets ($N_\text{jet}=;$2)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jjpT_dy_3j_excl
Title=Rapidity separation of leading jets ($N_\text{jet}=;$3)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jjdy_dy
Title=Rapidity separation of most forward and most backward jets
XLabel=$\Delta y (j_\text{fw},j_\text{bw})$ [GeV]
YLabel=d$\sigma$/d$\Delta y$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jjdy_dy_2j_excl
Title=Rapidity separation of most forward and most backward jets ($N_\text{jet}=;$3)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/jjdy_dy_3j_excl
Title=Rapidity separation of most forward and most backward jets ($N_\text{jet}=;$3)
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/tau_jet1
Title=$\tau$ of leading jet
XLabel=$\tau(j_1)$ [GeV]
YLabel=d$\sigma$/d$\tau$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/tau_jet2
Title=$\tau$ of subleading jet
XLabel=$\tau(j_2)$ [GeV]
YLabel=d$\sigma$/d$\tau$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/tau_jet3
Title=$\tau$ of 3rd jet
XLabel=$\tau(j_3)$ [GeV]
YLabel=d$\sigma$/d$\tau$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/sum_tau_jet
Title=Sum of $\tau$ of all jets ($\tau>\;$8 GeV)
XLabel=$\sum\limits_\text{jet}\tau$ [GeV]
YLabel=d$\sigma$/d$\sum\tau$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/tau_jet_max
Title=Maximal $\tau$ of all jets ($\tau>\;$8 GeV)
XLabel=$\tau_\text{max}$ [GeV]
YLabel=d$\sigma$/d$\tau_\text{max}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/xs_jet_veto_j0
Title=Jet veto cross section on leading jet
XLabel=$p_\perp^\text{veto}$ [GeV]
YLabel=$\sigma(p_\perp(j_1)<p_\perp^\text{veto}$ [pb]
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/xs_jet_veto_j1
Title=Jet veto cross section on subleading jet
XLabel=$p_\perp^\text{veto}$ [GeV]
YLabel=$\sigma(p_\perp(j_2)<p_\perp^\text{veto}$ [pb]
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/xs_jet_veto_h
Title=Jet veto cross section on subleading jet
XLabel=$p_\perp^\text{veto}$ [GeV]
YLabel=$\sigma(p_\perp(h)<p_\perp^\text{veto}$ [pb]
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_LH15/xs_central_jet_veto
Title=Jet cross section with $y_\text{dist}=\min\limits_\text{jets}\left|y_\text{jet}-\frac{y_\text{fw}+y_\text{bw}}{2}\right|<y_\text{dist}^\text{cut}$
XLabel=$y_\text{dist}^\text{cut}$
YLabel=$\sigma(y_\text{dist}<y_\text{dist}^\text{cut}$ [pb]
LogY=0
# END PLOT

# BEGIN SPECIAL /MC_HJETS_LH15/.*_jhj
\rput[lt]{0}(0.05,0.95){\bf central Higgs cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_VBF
\rput[lt]{0}(0.05,0.95){\bf VBF cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_VBF2
\rput[lt]{0}(0.05,0.95){\bf VBF2 cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_j1_30
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(j_1)>}\,$\bf 30 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_j1_50
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(j_1)>}\,$\bf 50 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_j1_100
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(j_1)>}\,$\bf 100 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_j1_200
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(j_1)>}\,$\bf 200 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_j1_500
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(j_1)>}\,$\bf 500 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_j1_1000
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(j_1)>}\,$\bf 1000 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_h_50
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(h)>}\,$\bf 50 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_h_100
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(h)>}\,$\bf 100 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_h_200
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(h)>}\,$\bf 200 GeV}
# END SPECIAL

# BEGIN SPECIAL /MC_HJETS_LH15/.*_h_500
\rput[lt]{0}(0.05,0.95){$\mathbf{p_\perp(h)>}\,$\bf 500 GeV}
# END SPECIAL


