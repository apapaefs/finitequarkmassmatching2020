// -*- C++ -*-
#include "Rivet/Analyses/MC_JetAnalysis.hh"
// #include "Rivet/Analysis.hh"
#include "Rivet/ParticleBase.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Logging.hh"

#include <map>

namespace Rivet {

  class HEJ_MT_VERIFY : public Analysis {

  #include "NLOHisto1D.cc"

  struct Hist_Info {
    size_t bins;
    double min,max;
    Hist_Info(size_t b, double mn, double mx) : bins(b),min(mn),max(mx) {}
  };
  typedef std::pair<std::string,Hist_Info> StrHistInfo;
  private:
    const double _jeta, _jR, _jpT; ///< jet cuts
    std::vector<double> _j1pTs;
    std::map<std::string,NLOHisto1DPtr> histos;
    int _current_event_number;
    const char _max_option = 'C'; ///< option for jet selection: A=All B=Higgs inside, C=Higgs outside
    /// values at minimal m12
    double minm12 = 99999.;
    double minR = 0.;
    double minpt1 = 0.;
    double minpt2 = 0.;
    double minm1 = 0.;
    double minm2 = 0.;
  public:
    HEJ_MT_VERIFY() :
      Analysis("HEJ_MT_VERIFY"),
      _jeta(4.4), _jR(0.4), _jpT(30.)
    {}

//{
    /// @name Initilize
    void init() {
      FinalState fs;
      addProjection(fs, "FS");

      IdentifiedFinalState hfinder(fs, PID::HIGGS);
      addProjection(hfinder, "Hfinder");

      IdentifiedFinalState gfinder(fs, PID::GLUON);
      addProjection(gfinder, "Gfinder");

      std::vector<int> quarks(13);
      std::iota(quarks.begin(), quarks.end(), -6);
      IdentifiedFinalState qfinder(fs, quarks);
      addProjection(qfinder, "Qfinder");
      qfinder.acceptId(PID::GLUON);
      addProjection(qfinder, "Partonfinder");

      VetoedFinalState hexcl(FinalState(Cuts::absetaIn(0.,_jeta)));
      hexcl.addVetoId(PID::HIGGS);
      addProjection(FastJets(hexcl, FastJets::ANTIKT, _jR), "Jets");

      inithistos();
    }

  private:
    void inithistos() {
      // dijet observables -> needed four times
      std::vector<StrHistInfo> obs;
      /// one particles onbservables
      obs.push_back(StrHistInfo(std::string("jj_dy_"),
                                Hist_Info(16,0.,8.)));
      obs.push_back(StrHistInfo(std::string("jH_dy_"),
                                Hist_Info(16,0.,8.)));
      obs.push_back(StrHistInfo(std::string("gH_dy_"),
                                Hist_Info(16,0.,8.)));
      obs.push_back(StrHistInfo(std::string("qH_dy_"),
                                Hist_Info(16,0.,8.)));

      for (char choice('A');choice<=_max_option;++choice) {
        std::string c(&choice);
        c=c.substr(0,1);
          for (size_t i(0);i<obs.size();++i) {
            std::string label(obs[i].first+c);
            histos[label] = bookNLOHisto1D(label,
                                          obs[i].second.bins,
                                          obs[i].second.min,
                                          obs[i].second.max);
        }
      }
      // [0] incl XS, [1] 2 tagjets XS, [2] h within tagjets XS,
      // [3] h outside tagjets XS
      histos["cutflow"] = bookNLOHisto1D("cutflow",4,-0.5,3.5);
    }
//}
  public:
    /// Do the analysis
    void analyze(const Event & e) {
      const double weight = e.weight();
      _current_event_number = e.genEvent()->event_number();

      histos["cutflow"]->fill(0.,weight,_current_event_number); // cutflow[0] -> tot.xsec
      // jets
      const FastJets& jetpro = applyProjection<FastJets>(e, "Jets");

      // partons
      const IdentifiedFinalState pfinder =
        applyProjection<IdentifiedFinalState>(e, "Partonfinder");
      const Particles& partons = pfinder.particles();

      if( jetpro.size() != 2 ) {
        std::cout << "--------- TOO FEW JETS -----" << std::endl;
        // std::cout <<  jetpro << std::endl;
        std::cout << "Partons:" << std::endl;
        for (const auto & part: partons){
          std::cout << part << " " << part.momentum().pT() << " "
            << part.momentum().rapidity() << std::endl;
        }
        std::cout << "dR " << deltaR(partons[0].momentum(), partons[1].momentum()) << std::endl;
        vetoEvent;
      }
      histos["cutflow"]->fill(1.,weight,_current_event_number); // cutflow[1] -> 2 jets

      // higgs
      const IdentifiedFinalState hfinder =
        applyProjection<IdentifiedFinalState>(e, "Hfinder");
      const Particle& higgs = hfinder.particles()[0];

      // quarks
      const IdentifiedFinalState qfinder =
        applyProjection<IdentifiedFinalState>(e, "Qfinder");
      const Particles& quarks = qfinder.particles();

      // quarks
      const IdentifiedFinalState gfinder =
        applyProjection<IdentifiedFinalState>(e, "Gfinder");
      const Particles& gluons = gfinder.particles();

      AnalyseConfiguration(higgs, gluons, quarks, partons, "A", weight);

      // define tagging of Higgs
      const double j1y = partons[0].momentum().rapidity();
      const double j2y = partons[1].momentum().rapidity();
      const double hy = higgs.momentum().rapidity();
      if ((hy>j1y && hy<j2y) || (hy<j1y && hy>j2y)) { // Higgs inside
        histos["cutflow"]->fill(2.,weight,_current_event_number); // cutflow[2] -> h inside
        AnalyseConfiguration(higgs, gluons, quarks, partons, "B", weight);
      } else {
        histos["cutflow"]->fill(3.,weight,_current_event_number); // cutflow[3] -> h outside
        AnalyseConfiguration(higgs, gluons, quarks, partons, "C", weight);
      }
    }

  private:
    void AnalyseConfiguration(const Particle& higgs, const Particles& gluons,
                              const Particles& quarks, const Particles& partons,
                              const std::string& c, const double& weight) {
      const double j1y = partons[0].momentum().rapidity();
      const double j2y = partons[1].momentum().rapidity();
      const double hy = higgs.momentum().rapidity();
      histos["jj_dy_"+c]->fill(abs(j1y-j2y),weight,_current_event_number);
      double dyHj_min = 3.*_jeta;
      for(const auto & parton: partons){
        const double dyHj = abs(parton.momentum().rapidity()-hy);
        if ( dyHj < dyHj_min ) dyHj_min = dyHj;
      }
      histos["jH_dy_"+c]->fill(dyHj_min,weight,_current_event_number);
      if(gluons.size() != 0){
        double dyHg_min = 3.*_jeta;
        for(const auto & gluon: gluons){
          const double dyHg = abs(gluon.momentum().rapidity()-hy);
          if ( dyHg < dyHg_min ) dyHg_min = dyHg;
        }
        histos["gH_dy_"+c]->fill(dyHg_min,weight,_current_event_number);
      }
      if(quarks.size() != 0){
        double dyHq_min = 3.*_jeta;
        for(const auto & quark: quarks){
          const double dyHq = abs(quark.momentum().rapidity()-hy);
          if ( dyHq < dyHq_min ) dyHq_min = dyHq;
        }
        histos["qH_dy_"+c]->fill(dyHq_min,weight,_current_event_number);
      }
    }

  public:
    /// Finalize
    void finalize() {
      MSG_INFO("Compiled on " << __DATE__ << " " << __TIME__);
      double scalefactor(crossSection()/sumOfWeights());

      /// rescale everything to XS
      for (auto hit=histos.begin(); hit!=histos.end();hit++)
        scale(hit->second,scalefactor);
    }
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(HEJ_MT_VERIFY);
}
