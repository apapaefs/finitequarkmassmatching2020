# BEGIN PLOT /HEJ_MT_VERIFY
#RatioPlotMode=deviation
#RatioPlotYMin=-10
RatioPlotMode=default
RatioPlotYMax=1.2
RatioPlotYMin=0.8
# END PLOT

# BEGIN PLOT /HEJ_MT_VERIFY/cutflow
Title=Cutflow of VBF cuts
XLabel=
YLabel=$\sigma$ [pb]
XCustomMajorTicks=0 total 1 2jet 2 h-within 3 h-outside
XMin=-0.5
XMax=3.5
# END PLOT

# BEGIN PLOT /HEJ_MT_VERIFY/jj_dy
Title=Rapidity separation of the two leading jets
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /HEJ_MT_VERIFY/jH_dy
Title=Minimal Rapidity separation between Higgs and Jets
XLabel=$\min(\Delta y(j_i,h))$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /HEJ_MT_VERIFY/gH_dy
Title=Minimal Rapidity separation between Higgs and Gluon
XLabel=$\min(\Delta y(g_i,h))$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /HEJ_MT_VERIFY/qH_dy
Title=Minimal Rapidity separation between Higgs and Quarks
XLabel=$\min(\Delta y(q_i,h))$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LogY=1
# END PLOT

# BEGIN SPECIAL /HEJ_MT_VERIFY/.*_A
\rput[lt]{0}(0.05,0.95){\bf Any Higgs}
# END SPECIAL

# BEGIN SPECIAL /HEJ_MT_VERIFY/.*_B
\rput[lt]{0}(0.05,0.95){\bf Higgs inside}
# END SPECIAL

# BEGIN SPECIAL /HEJ_MT_VERIFY/.*_C
\rput[lt]{0}(0.05,0.95){\bf Higgs outside}
# END SPECIAL
