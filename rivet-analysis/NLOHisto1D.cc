#include "YODA/Histo1D.h"

class NLOHisto1D : public YODA::Histo1D {

YODA::Histo1D* _tmphist;
int _current_event_number;

void _syncHists() {
  for (size_t i=0; i<_tmphist->bins().size(); ++i) {
    if (_tmphist->bin(i).area()) YODA::Histo1D::fillBin(i, _tmphist->bin(i).area());
  }
    if (_tmphist->overflow().sumW())  YODA::Histo1D::overflow()+=_tmphist->overflow();
    if (_tmphist->underflow().sumW()) YODA::Histo1D::underflow()+=_tmphist->underflow();
    _tmphist->reset();
  }

void _checkEveNumber(const int event_number){
    if (_current_event_number==-1)
      _current_event_number = event_number;

    if (event_number !=_current_event_number) {
      _syncHists();
      _current_event_number = event_number;
    }
}

public:

  NLOHisto1D(size_t nbins, double lower, double upper, const string& path) :
    YODA::Histo1D(nbins, lower, upper, path),
    _current_event_number(-1)
  {
    _tmphist = new Histo1D(nbins, lower, upper, path+"_tmp");
  }

  NLOHisto1D(const vector<double>& binedges, const string& path) :
    YODA::Histo1D(binedges, path),
    _current_event_number(-1)
  {
    _tmphist = new Histo1D(binedges, path+"_tmp");
  }

  ~NLOHisto1D()
  {
    delete _tmphist;
  }

  void fill(const double x, const double weight, const int event_number)
  {
    _checkEveNumber(event_number);
    _tmphist->fill(x, weight);
  }
  void fill(const double x, const Event& event, const double fac = 1.)
  {
    fill(x, event.weight()*fac, event.genEvent()->event_number());
  }

  void fillBin(const size_t i, const double weight, const int event_number)
  {
    _checkEveNumber(event_number);
    _tmphist->fillBin(i, weight);
  }
  void fillBin(const size_t i, const Event& event, const double fac = 1.)
  {
    fillBin(i, event.weight()*fac, event.genEvent()->event_number());
  }

  /// Fill a bin with absolute value, independent of bin width
  void fillAbs(const double x, const double weight, const int event_number){
    const ssize_t index = binIndexAt(x);
    if(index == -1) fill(x, weight, event_number);
    else {
      fillBin(index, weight*_tmphist->bin(index).xWidth(), event_number);
    }
  }
  void fillAbs(const double x, const Event& event, const double fac = 1.){
    fillAbs(x, event.weight()*fac, event.genEvent()->event_number());
  }

  void finalize()
  {
    _syncHists();
  }

  /// Access a bin index by coordinate
  ssize_t binIndexAt(double x) const {
    return _tmphist->binIndexAt(x);
  }
  /// Number of bins on this axis (not counting under/overflow)
  size_t numBins() const { return _tmphist->numBins(); }

  /// All bin edges on this histo's axis
  const std::vector<double> xEdges() const { return _tmphist->xEdges(); }
};

typedef shared_ptr<NLOHisto1D> NLOHisto1DPtr;


NLOHisto1DPtr bookNLOHisto1D(const string& hname,
                                   size_t nbins, double lower, double upper)
{
  NLOHisto1DPtr hist( new NLOHisto1D(nbins, lower, upper, histoPath(hname)) );
  addAnalysisObject(hist);
  return hist;
}

NLOHisto1DPtr bookNLOHisto1D(const string& hname, const vector<double>& binedges)
{
  NLOHisto1DPtr hist( new NLOHisto1D(binedges, histoPath(hname)) );
  addAnalysisObject(hist);
  return hist;
}
