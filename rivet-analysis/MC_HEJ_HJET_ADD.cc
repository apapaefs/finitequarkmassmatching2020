// -*- C++ -*-
#include "Rivet/Analyses/MC_JetAnalysis.hh"
// #include "Rivet/Analysis.hh"
#include "Rivet/ParticleBase.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Logging.hh"

#include <map>
#include <algorithm>
// #include <sstream>

/*
* @brief Analysis for h(-> yy) + jets
* @note based on <a href="https://phystev.cnrs.fr/wiki/2013:groups:sm:higgs:hdijets">MC_HDIJETS_LESHOUCHES</a>
*/

namespace Rivet {

  /// @brief MC validation analysis for higgs [-> gamma gamma] + jets events
  class MC_HEJ_HJET_ADD : public Analysis {

  #include "NLOHisto1D.cc"

  struct Hist_Info {
    size_t bins;
    double min,max;
    Hist_Info(size_t b, double mn, double mx) : bins(b),min(mn),max(mx) {}
  };
  typedef std::pair<std::string,Hist_Info> StrHistInfo;
  private:
    const double _jeta, _jR, _jpT, _jjdy, _jjmass, _jdR; ///< jet cuts
    const double _yy, _ymmin,_ymmax, _y1pt, _y2pt; ///< photon cuts
    const double _BR; ///< Branching ratio
    double _sum_accepted_weight = 0;
    std::vector<double> _j1pTs;
    std::map<std::string,NLOHisto1DPtr> histos; //, jetvetohistos;
    const char _max_option; ///< option for jet selection: A=hardest, B=forward/backward
    int _current_event_number;
  public:
    MC_HEJ_HJET_ADD() :
      Analysis("MC_HEJ_HJET_ADD"),
      _jeta(4.4), _jR(0.4), _jpT(30.), _jjdy(2.8), _jjmass(400.), _jdR(5.0),
      _yy(2.37), _ymmin(105.), _ymmax(160.), _y1pt(0.35), _y2pt(0.25),
      //_BR(0.00236),
      _BR(1.),
      _max_option('D')
    {}

//{
    /// @name Initilize
    void init() {
      FinalState fs;
      addProjection(fs, "FS");
      IdentifiedFinalState hfinder(fs, PID::HIGGS);
      addProjection(hfinder, "Hfinder");
      IdentifiedFinalState pfinder(fs, PID::PHOTON);
      addProjection(pfinder, "Photonfinder");
      //hfinder.acceptId(PID::HIGGS);
      // VetoedFinalState hexcl(FinalState(-_jeta,_jeta,0.*GeV));
      VetoedFinalState hexcl(FinalState(Cuts::absetaIn(0.,_jeta)));
      hexcl.addVetoId(PID::HIGGS);
      hexcl.addVetoId(PID::PHOTON);
      addProjection(FastJets(hexcl, FastJets::ANTIKT, _jR), "Jets");

      inithistos();
    }

  private:
    void inithistos() {
      // inclusive and one-jet observables
      histos["NJet_incl"]    = bookNLOHisto1D("NJet_incl",5,-0.5,4.5);
      // dijet observables -> needed four times
      std::vector<StrHistInfo> obs;
      /// one particles onbservables
      obs.push_back(StrHistInfo(std::string("H_pT_"),
                                Hist_Info(50,0.,500.)));

      obs.push_back(StrHistInfo(std::string("H_y_"),
                                Hist_Info(20,-5.,5.)));
      /// delta y
      obs.push_back(StrHistInfo(std::string("jet12_dy_"),
                                Hist_Info(16,0.,8.)));
      obs.push_back(StrHistInfo(std::string("jet12_njy_"),
                                Hist_Info(16,0.,8.)));
      obs.push_back(StrHistInfo(std::string("jet12_njy_in_"),
                                Hist_Info(16,0.,8.)));

      /// jet1+jet2
      obs.push_back(StrHistInfo(std::string("jet12_dphi_"),
                                Hist_Info(62,-3.1,3.1)));
      obs.push_back(StrHistInfo(std::string("jet12_mass_"),
                                Hist_Info(20,0.,1000.)));

      /// HT/2
      obs.push_back(StrHistInfo(std::string("HT2_"),
                                Hist_Info(50.,0.,500.)));

      /// y_c
      obs.push_back(StrHistInfo(std::string("yc_"),
                                Hist_Info(40.,0.,4.)));

      /// dR_c
      obs.push_back(StrHistInfo(std::string("drc_"),
                                Hist_Info(60.,0.,6.)));

      {
        std::vector<std::string> type;
        type.push_back("dijet_");
        type.push_back("VBF_");
        for (char choice('A');choice<=_max_option;++choice) {
          std::string c(&choice);
          c=c.substr(0,1);
          for (size_t j(0);j<type.size();++j) {
            for (size_t i(0);i<obs.size();++i) {
              std::string label(obs[i].first+type[j]+c);
	      //cout << "label = " << label << " " << obs[i].second.bins << " " << obs[i].second.min << " " << obs[i].second.max << endl;
              histos[label] = bookNLOHisto1D(label,
                                            obs[i].second.bins,
                                            obs[i].second.min,
                                            obs[i].second.max);
            }
          }
          // [0] dijet XS, [1] h within tagjets XS,
          // [2] VBF cuts XS, [3] h within tagjets XS
          std::string hw("XS_H_within_tagjets_");
          histos[hw+c] = bookNLOHisto1D(hw+c,4,-0.5,3.5);
        }
      }
      // [0] incl XS, [1] 2 tagjets XS, [2] 3rd jet XS,
      // [3] VBF cuts XS, [4] 3rd jet in VBF cuts XS
      histos["cutflow"] = bookNLOHisto1D("cutflow",6,-0.5,5.5);
    }
//}
  public:
    /// Do the analysis
    void analyze(const Event & e) {
      const double weight = e.weight();
      _current_event_number = e.genEvent()->event_number();

      const IdentifiedFinalState hfinder =
        applyProjection<IdentifiedFinalState>(e, "Hfinder");
      histos["cutflow"]->fillAbs(0.,weight,_current_event_number); // cutflow[0] -> tot.xsec
      // jets
      const PseudoJets jets = applyProjection<FastJets>(e, "Jets").pseudoJetsByPt(_jpT*GeV);
      // Jets& jets = jetpro.jetsByPt(_jpT*GeV);

      // higgs
      Particle temphiggs;
      if (hfinder.size()!=1) { //no Higgs found -> reconstruct from 2 photons
	const Particles& photons =
        applyProjection<IdentifiedFinalState>(e, "Photonfinder").particlesByPt();
        if(PhotonCuts(photons, jets))
          temphiggs = Particle(PID::HIGGS, photons[0].momentum()+photons[1].momentum());
        else vetoEvent;
      }
      else{
	//cout << "Higgs found!" << endl;
         temphiggs = hfinder.particles()[0];
      }
      Particle higgs(temphiggs);
      _sum_accepted_weight+=weight;

      histos["cutflow"]->fillAbs(1.,weight,_current_event_number); // cutflow[1] -> incl.xsec

      for (size_t i(0);i<5;++i) {
        if (jets.size()>=i) histos["NJet_incl"]->fill(i,weight,_current_event_number);
      }
      // define tagging jets
      if (jets.size()>1) {
        histos["cutflow"]->fillAbs(2.,weight,_current_event_number); // cutflow[2] -> 2 tagjets
        if( jets.size()>2 )
          histos["cutflow"]->fillAbs(3.,weight,_current_event_number); // cutflow[3] -> 3rd jet
        const bool passedVBF = PassVBFCut(jets);
        if( passedVBF ) {
          histos["cutflow"]->fillAbs(4.,weight,_current_event_number); // cutflow[4] -> VBF
          if( jets.size()>2 )
            histos["cutflow"]->fillAbs(5.,weight,_current_event_number); // cutflow[5] -> 3rd jet VBF
        }
        for (char choice('A');choice<=_max_option;++choice) {
          std::string c(&choice); c=c.substr(0,1);
          // choice 'A' hardest jets
          PseudoJet tagjet1 = jets[0];
          PseudoJet tagjet2 = jets[1];
          if( choice>'B' && jets.size()<3 ) continue; // choice 'C'/'D' > 3jet
          if (choice=='B'|| choice=='D') { // choice 'B'/'D' forward-backward
            for (size_t i(0);i<jets.size();++i) {
              if (jets[i].rapidity()
                    < tagjet1.rapidity()) {
                tagjet1 = jets[i];
              }
              else if (jets[i].rapidity()
                         > tagjet2.rapidity()) {
                tagjet2 = jets[i];
              }
            }
            if (tagjet1.pt() < tagjet2.pt()) {
              std::swap(tagjet1,tagjet2);
            }
          }
          AnalyseConfiguration(higgs,tagjet1,tagjet2,jets,c,weight,passedVBF);
        }
      }
    }

  private:
    bool PhotonCuts( const Particles& photons, const PseudoJets& jets ){
      if( photons.size() != 2) return false;
      const auto & mom1 = photons[0].momentum();
      const auto & mom2 = photons[1].momentum();
      if( mom1.absrap() > _yy || mom2.absrap() > _yy)
        return false;
      double massij(( mom1 + mom2 ).mass());
      if( massij < _ymmin || massij > _ymmax ) return false;
      if(mom1.pT() < _y1pt*massij || mom2.pT() < _y2pt*massij)
        return false;
      if( _jR > Rdist(mom1,mom2) ) return false;
      for( const auto& jet: jets){
        if( _jR > Rdist(momentum(jet),mom1) || _jR > Rdist(momentum(jet),mom2) )
          return false;
      }
      return true;
    }

    void AnalyseConfiguration(Particle higgs, PseudoJet jet1,
                              PseudoJet jet2, PseudoJets jets,
                              const std::string& c, const double& weight, const bool passedVBF) {
      // h ........ Higgs
      // jet1 ..... leading tagjet (in either definition A or B)
      // jet2 ..... subleading tagjet (in either definition A or B)
      // jets ..... all jets to find suitably defined third jet
      // c ........ either 'A' or 'B'
      // weight ... weight of the event
      //
      // dijet observables
      const FourMomentum& h = higgs.momentum();
      const FourMomentum& j1 = momentum(jet1);
      const FourMomentum& j2 = momentum(jet2);
      //cout << "test11" << endl;
      FillHiggsWithinDijetsHisto(h.rapidity(),j1.rapidity(),j2.rapidity(),
         c,0,weight);
      //cout << h.rapidity() << " " << j1.rapidity() << " " << j2.rapidity() << " " << c << " " << weight << endl;
      //cout << "test12" << endl;
      FillDijetHistos(h,j1,j2,c,"dijet",weight);
      FillNjetHistos(j1,j2,jets,"dijet_"+c,weight);
      FillYcHistos(jet1,jet2,jets,"dijet_"+c,weight);
      FilldeltaRcHistos(jet1,jet2,jets,"dijet_"+c,weight);
      if (jets.size()>2) {
        // hardest jet which is non-tagjet, jets are already sorted by pT
        // -> first non-tagjet is hardest
        // -> always find one
        size_t idx(0);
        for (size_t i(0);i<jets.size();++i) {
          if (&jets[i]!=&jet1 && &jets[i]!=&jet2) {
            idx = i;
            break;
          }
        }
        const FourMomentum& j3(momentum(jets[idx]));
        FillThreejetHistos(h,j1,j2,j3,c,"dijet",weight);
      }
      // VBF observables
      if (passedVBF) {
        FillHiggsWithinDijetsHisto(h.rapidity(),j1.rapidity(),j2.rapidity(),
           c,2,weight);
        FillDijetHistos(h,j1,j2,c,"VBF",weight);
        FillNjetHistos(j1,j2,jets,"VBF_"+c,weight);
        FillYcHistos(jet1,jet2,jets,"VBF_"+c,weight);
	FilldeltaRcHistos(jet1,jet2,jets,"VBF_"+c,weight);
        if (jets.size()>2) {
          // hardest jet which is non-tagjet and inbetween j1 and j2,
          // jets are already sorted by pT -> first non-tagjet is hardest
          // possibility not to find one in choice A -> nothing to do
          size_t idx(0);
          for (size_t i(0);i<jets.size();++i) {
            if (&jets[i]!=&jet1 && &jets[i]!=&jet2) {
              const FourMomentum& j(momentum(jets[i]));
              if ((j.rapidity()>j1.rapidity() && j.rapidity()<j2.rapidity()) ||
                  (j.rapidity()<j1.rapidity() && j.rapidity()>j2.rapidity())) {
                idx = i;
                break;
              }
            }
            if (i==jets.size()-1) return;
          }
          const FourMomentum& j3(momentum(jets[idx]));
          FillThreejetHistos(h,j1,j2,j3,c,"VBF",weight);
        }
      }
    }

    bool PassVBFCut(const PseudoJets& jets){
      const FourMomentum& j0 = momentum(jets[0]);
      const FourMomentum& j1 = momentum(jets[1]);
      return fabs(j0.rapidity()-j1.rapidity())>_jjdy && (j0+j1).mass()>_jjmass;
    }

    void FillHiggsWithinDijetsHisto(const double& hy, const double& j1y,
            const double& j2y,
            const string& c, const size_t& i,
            const double& weight) {
      //cout << "test1" << endl;
      histos["XS_H_within_tagjets_"+c]->fill(i,weight,_current_event_number);
      //cout << "test2" << endl;
      if ((hy>j1y && hy<j2y) || (hy<j1y && hy>j2y)) {
        histos["XS_H_within_tagjets_"+c]->fill(i+1,weight,_current_event_number);
      }
      //cout << "test3" << endl;
    }

    void FillDijetHistos(const FourMomentum& h, const FourMomentum& j1,
       const FourMomentum& j2,
       const string& c, const string& type,
       const double& weight) {
      double massjj((j1+j2).mass());
      double dyjj(fabs(j1.rapidity()-j2.rapidity()));
      double dphi(j1.phi()-j2.phi()); // deltaPhi(j1,j2)
      if( dphi > PI ) dphi -= 2*PI;
      if( dphi < -PI) dphi += 2*PI;

      histos["H_pT_"+type+"_"+c]->fill(h.pT(),weight,_current_event_number);
      histos["H_y_"+type+"_"+c]->fill(h.rapidity(),weight,_current_event_number);
      histos["jet12_dy_"+type+"_"+c]->fill(dyjj,weight,_current_event_number);
      histos["jet12_dphi_"+type+"_"+c]->fill(dphi,weight,_current_event_number);
      histos["jet12_mass_"+type+"_"+c]->fill(massjj,weight,_current_event_number);
    }

    void FillThreejetHistos(const FourMomentum& h, const FourMomentum& j1,
          const FourMomentum& j2, const FourMomentum& j3,
          const string& c, const string& type,
          const double& weight) {
    }

    void FillNjetHistos(const PseudoJet& jet1, const PseudoJet& jet2, const PseudoJets& jets,
                          const std::string& c, const double& weight){
      double HT(0.);
      for (auto jet: jets){
        HT+=jet.pt();
      }
      histos["HT2_"+c]->fill(HT/2., weight,_current_event_number);

      size_t inside(2+BetweenJets(jet1, jet2, jets));
      double dyjj(fabs(jet1.rapidity()-jet2.rapidity()));
      histos["jet12_njy_in_"+c]->fill(dyjj,inside*weight,_current_event_number);
      histos["jet12_njy_"+c]->fill(dyjj,jets.size()*weight,_current_event_number);
    }

    void FillYcHistos(const PseudoJet& jet1, const PseudoJet& jet2, const PseudoJets& jets,
                          const std::string& c, const double weight){
      double y_min = jet1.rapidity();
      double y_max = jet2.rapidity();
      double yc_max = 2.1*_jeta;
      if(y_min > y_max) std::swap(y_min, y_max);
      const double y_mid = (y_min + y_max)/2.;
      for(const auto & jet: jets){
        // ignore tagging jets
        if( jet1.user_index() == jet.user_index()
          || jet2.user_index() == jet.user_index() )
          continue;
        const double y_jet = jet.rapidity();
        // ignore jets outside tagging
        if(y_jet >= y_max || y_jet <= y_min) continue;
        if(yc_max > abs(y_jet-y_mid)) {
          yc_max = abs(y_jet-y_mid);
        }
      }
      FillUntil(histos["yc_"+c], yc_max, weight);
    }

    void FilldeltaRcHistos(const PseudoJet& jet1, const PseudoJet& jet2, const PseudoJets& jets,
                          const std::string& c, const double weight){
      //double y_min = jet1.rapidity();
      //double y_max = jet2.rapidity();
      double deltaRc_max = -1.0;
      for(const auto & jet: jets){
        // ignore tagging jets
        if( jet1.user_index() == jet.user_index()
          || jet2.user_index() == jet.user_index() )
          continue;
        //const double y_jet = jet.rapidity();
	const double deltaR1_jet = abs(jet1.delta_R(jet));
	const double deltaR2_jet = abs(jet2.delta_R(jet));
	deltaRc_max = std::max(abs(deltaR1_jet), abs(deltaR2_jet));	
      }
      //cout << "deltaRc_max = " << deltaRc_max << endl;
      FillFrom(histos["drc_"+c], deltaRc_max, weight);
    }

    /// fills a hist up to value
    void FillUntil(NLOHisto1DPtr hist, const double value, const double weight){

      for(const double edge: hist->xEdges()){
	//cout << "edge until = " << edge << endl;
        if(value >= edge) { hist->fillAbs(edge, weight, _current_event_number); /*cout << "value, edge" << value << ", " << edge << endl;*/ }
        else return;
      }
    }

    /// fills a hist from a value
    void FillFrom(NLOHisto1DPtr hist, const double value, const double weight){
      //cout << "hist->xEdges() " << hist->xEdges() << " value=" << value << endl;
      for(int ei=hist->xEdges().size()-1; ei>=0; --ei){
	double edge = hist->xEdges()[ei];
	//cout << "hist->xEdges().size(), ei, edge " << hist->xEdges().size() << " " << ei << ", " << edge << " value = " << value << endl;
        if(value < edge) { hist->fillAbs(edge, weight, _current_event_number); /*cout << "value, edge = " << value << ", " << edge << endl;*/ } 
        else return;
      }
    }

    int BetweenJets(const PseudoJet& jet1, const PseudoJet& jet2, const PseudoJets& jets){
      if(jets.size() < 3) return 0;
      double ymin = jet1.rap();
      double ymax = jet2.rap();
      if(ymin > ymax) std::swap(ymin, ymax);
      return std::count_if(
          begin(jets), end(jets),
          [ymin, ymax](PseudoJet const & jet){
            return ymin < jet.rapidity() && jet.rapidity() < ymax;
          }
      );
    }

    double Rdist(const FourMomentum& mom1, const FourMomentum& mom2){
      double distance = min(mom1.pt2(), mom1.pt2());
      double dphi = abs(mom1.phi() - mom2.phi());
      if (dphi > pi) {dphi = twopi - dphi;}
      double drap = mom1.rap() - mom2.rap();
      distance = distance * (dphi*dphi + drap*drap);
      return distance;
    }

  public:
    /// Finalize
    void finalize() {
      MSG_INFO("Compiled on " << __DATE__ << " " << __TIME__);
      if(crossSection()==-1.) {
        MSG_INFO("xs=" << crossSection() << " overwritten by sum of weights=" << sumOfWeights());
        setCrossSection(sumOfWeights());
      } else {
        MSG_INFO("rescaling XS " << crossSection() << " to BR " << _BR <<
          " => " << crossSection()*_BR);
        setCrossSection(crossSection()*_BR);
      }
      MSG_INFO("xs after photon cut " <<  crossSection()*_sum_accepted_weight/sumOfWeights()
        << " (" << _sum_accepted_weight/sumOfWeights()*100. << "%)");
      MSG_INFO("xs=" << crossSection() << " SumOfWeights=" << sumOfWeights() << " #events=" << numEvents());
      double scalefactor(crossSection()/sumOfWeights());

      /// rescale everything to XS
      for (auto hit=histos.begin(); hit!=histos.end();hit++) {
        hit->second->finalize();
        scale(hit->second,scalefactor);
      }
    }
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_HEJ_HJET_ADD);
}
