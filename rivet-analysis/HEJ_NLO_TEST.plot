# BEGIN PLOT /HEJ_NLO_TEST/jet12
LogY=0
# END PLOT


# BEGIN PLOT /HEJ_NLO_TEST/jet12_dy
Title=Rapidity separation of the two leading jets
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/jet12_dphi
Title=Azumuthal separation of the two leading jets
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LegendYPos=0.75
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/jet12_mass
Title=Invariant mass of the two tagging jets
XLabel=$m_{jj}$ [GeV]
YLabel=d$\sigma$/d$m_{jj}$ [pb/GeV]
# END PLOT

# BEGIN SPECIAL /HEJ_NLO_TEST/.*_real
\rput[lt]{0}(0.05,0.95){\bf pure real}
# END SPECIAL

# BEGIN SPECIAL /HEJ_NLO_TEST/.*_virt
\rput[lt]{0}(0.05,0.95){\bf pure virtual}
# END SPECIAL

# BEGIN SPECIAL /HEJ_NLO_TEST/.*_both
\rput[lt]{0}(0.05,0.95){\bf virtual+real}
# END SPECIAL

# BEGIN SPECIAL /HEJ_NLO_TEST/.*_neg
\rput[lt]{0}(0.05,0.88){\bf only negative weights}
# END SPECIAL

# BEGIN SPECIAL /HEJ_NLO_TEST/.*_all
\rput[lt]{0}(0.05,0.88){\bf all weights}
# END SPECIAL

# BEGIN SPECIAL /HEJ_NLO_TEST/.*_pos
\rput[lt]{0}(0.05,0.88){\bf only positive weights}
# END SPECIAL

# BEGIN PLOT /HEJ_NLO_TEST/.*_pos
LogY=1
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/.*_neg
LogY=1
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/N
LogY=1
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/NNon_Jet
LogY=0
#YMin=-0.2
#YMax=0.6
#NormalizeToIntegral=1
XLabel=Num. Particles outside Jets
YLabel=d$\sigma$/d$(n\leq N_{non jet})$ [pb]
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/NNon_Jet_exc
LogY=0
#YMin=-0.2
#YMax=0.6
#NormalizeToIntegral=1
YLabel=d$\sigma$/d$(n=N_{non jet})$ [pb]
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/NParticles
LogY=0
#YMin=-0.2
#YMax=0.6
#NormalizeToIntegral=1
XLabel=Num. Particles
YLabel=d$\sigma$/d$(n\leq N_{part})$ [pb]
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/NParticles_exc
LogY=0
#YMin=-0.1
#YMax=0.7
YLabel=d$\sigma$/d$(n=N_{part})$ [pb]
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/exponential
LogY=1
#NormalizeToIntegral=1
XLabel=$\omega \alpha_s$
YLabel=$1/\sigma$d$\sigma$/d$(\omega \alpha_s)$ [pb]
LegendXPos=0.6
LegendYPos=0.6
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/exponential_abs
YLabel=$\%$ Events
#NormalizeToSum=1
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/alpha_s
LogY=1
YLabel=d$\sigma$/d$(\alpha_s)$ [pb]
XLabel=$\alpha_s$
# END PLOT

# BEGIN PLOT /HEJ_NLO_TEST/alpha_s_abs
LogY=1
YLabel=$\%$ Events
# END PLOT
