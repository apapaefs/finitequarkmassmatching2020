// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/Beam.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
// #include "YODA/Histo1D.h"

namespace Rivet {


  /// @brief Test Analysis for jet clustering of HEJ
  class HEJ_NLO_TEST : public Analysis {
  private:

    #include "NLOHisto1D.cc"

    std::string choices_emi[3] = {"both", "virt", "real"}; ///< choices for the extra emission
    std::string choices_wt[3] = {"all", "neg", "pos"}; ///< choices for the weight selection

    /// @brief struct to store general histo infos
    struct Hist_Info {
      size_t bins;
      double min,max;
      Hist_Info(size_t b, double mn, double mx) : bins(b),min(mn),max(mx) {}
    };
    typedef std::pair<std::string,Hist_Info> StrHistInfo;
    int _current_event_number;

    /// @name global options
    ///@{
    const double _jeta, _jR, _jpT; ///< jet cuts
    const bool _print_events; ///< switch print every event
    // std::map<std::string,Histo1DPtr> histos; ///< jetvetohistos
    std::map<std::string,NLOHisto1DPtr> histos; ///< jetvetohistos
    ///@}
    double gen_minpt = 100000.;
    double gen_minpt_jet = 100000.;

    double gen_minscale = 100000000.;
    double gen_maxscale = 0.;

    const double LAMBDA = 2.; ///< lambda as used by HEJ

  public:

    /// Constructor
    // DEFAULT_RIVET_ANALYSIS_CTOR(HEJ_NLO_TEST);
    HEJ_NLO_TEST() :
      Analysis("HEJ_NLO_TEST"),
      _jeta(4.4), _jR(0.4), _jpT(30.),
      _print_events(false)
    {}


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections
      Beam beam;
      addProjection(beam, "beam");
      FinalState fs;
      addProjection(fs,"FS");
      IdentifiedFinalState hfinder(fs, PID::HIGGS);
      addProjection(hfinder, "Hfinder");
      IdentifiedFinalState pfinder(fs, PID::PHOTON);
      addProjection(pfinder, "Photonfinder");
      VetoedFinalState hexcl(fs);
      hexcl.addVetoId(PID::HIGGS);
      hexcl.addVetoId(PID::PHOTON);
      addProjection(hexcl,"partons");
      addProjection(FastJets(hexcl, FastJets::ANTIKT, _jR), "Jets");

      YODA::Histo1D test;

      // Book histograms
      inithistos();
    }

    /// Perform the per-event analysis
    void analyze(const Event& ev) {
      const Jets& jets = applyProjection<FastJets>(ev, "Jets").jetsByPt(_jpT*GeV);
      if( jets.size() < 2 ) {
        if(_print_events) MSG_WARNING("Non 2j event");
        // print_event(ev);
        vetoEvent;
      }

      double wt(ev.weight());
      _current_event_number = ev.genEvent()->event_number();

      histos["alpha_s"]->fill(ev.genEvent()->alphaQCD(),wt,_current_event_number);
      histos["alpha_s_abs"]->fill(ev.genEvent()->alphaQCD(),1,_current_event_number);
      {
        double scale(ev.genEvent()->event_scale());
        if(scale == -1.){
          const auto & w_cont = ev.genEvent()->weights();
          // MSG_INFO("Looking inside w container " << w_cont.size());
          // w_cont.print();
          if(w_cont.has_key("MuR2")) scale = sqrt(w_cont["MuR2"]);
        }
        // MSG_INFO("event scale " << scale);
        histos["event_scale"]->fill(scale,wt,_current_event_number);
        histos["event_scale_abs"]->fill(scale,1,_current_event_number);
        if(scale<gen_minscale) gen_minscale = scale;
        else if(scale>gen_maxscale) gen_maxscale = scale;
      }

      // different partons
      const auto& beam = applyProjection<Beam>(ev, "beam").beams();
      // const FastJets& jetpro = applyProjection<FastJets>(ev, "Jets");
      const auto& part = applyProjection<FinalState>(ev, "partons").particlesByPt();
      const auto& non_jet( non_jet_part(jets, part));

      if(_print_events){
        print_event(ev);
        MSG_INFO("Non jet particles:");
        print_parts(non_jet);
      }

      if(part.back().pt() < gen_minpt) gen_minpt = part.back().pt();
      if(jets.back().pt() < gen_minpt_jet) gen_minpt_jet = jets.back().pt();

      // fill #particles histos
      for(size_t i(0); i<=jets.size(); ++i)
        histos["NJet"]->fill(i, wt,_current_event_number);
      for(size_t i(0); i<=part.size(); ++i)
        histos["NParticles"]->fill(i, wt,_current_event_number);
      for(size_t i(0); i<=non_jet.size(); ++i)
        histos["NNonJet"]->fill(i, wt,_current_event_number);
      histos["NJet_exc"]->fill(jets.size(), wt,_current_event_number);
      histos["NParticles_exc"]->fill(part.size(), wt,_current_event_number);
      histos["NNonJet_exc"]->fill(non_jet.size(), wt,_current_event_number);

      /// plot the value of the virtual correction
      double virtual_correction = virtual_exp(part, beam, ev.genEvent()->alphaQCD());
      histos["exponential"]->fill(virtual_correction, wt,_current_event_number);
      histos["exponential_abs"]->fill(virtual_correction, 1,_current_event_number);
      double expansion = 1.;
      int factorial = 1;
      histos["virtual_exp"]->fill(0, wt,_current_event_number);
      histos["virtual_exp_abs"]->fill(0, 1,_current_event_number);
      for(int order = 1; order <= 20; ++order ){
        factorial *= order;
        expansion += pow(virtual_correction,order)/factorial;
        histos["virtual_exp"]->fill(order, expansion*wt,_current_event_number);
        histos["virtual_exp_abs"]->fill(order, expansion,_current_event_number);
      }
      histos["virtual_exp"]->fill(21.,exp(virtual_correction)*wt,_current_event_number);
      histos["virtual_exp_abs"]->fill(21., exp(virtual_correction),_current_event_number);

      /// Fill the choice specific histograms
      fillHistos(jets, wt, choices_emi[0], choices_wt[0]);

      std::string ch_emi = choices_emi[2];
      if(non_jet.size() == 0) ch_emi = choices_emi[1];
      fillHistos(jets, wt, ch_emi, choices_wt[0]);

      std::string ch_wt = choices_wt[2];
      if(wt<0) {
        ch_wt = choices_wt[1];
        wt = - wt;
      }
      fillHistos(jets, wt, choices_emi[0], ch_wt);

      fillHistos(jets, wt, ch_emi, ch_wt);

    }


    /// Normalise histograms etc., after the run
    void finalize() {
      MSG_INFO("Compiled on " << __DATE__ << " " << __TIME__);
      MSG_INFO("total xs " << crossSection() << " sum of weight " << sumOfWeights());
      MSG_INFO("minmal parton pt parton: " << gen_minpt << " jets: " << gen_minpt_jet);
      MSG_INFO("event scale min: " << gen_minscale << " max: " << gen_maxscale);

      // scale everything to xs
      double scalefactor(crossSection()/sumOfWeights());
      MSG_INFO("Scalefactor " << scalefactor);
      for (auto hit=histos.begin(); hit!=histos.end();hit++){
        hit->second->finalize();
        scale(hit->second,scalefactor);
      }
      // @TODO there is something odd going on ...
      //       why is this result not the same as MC_HEJ_HJET? and why are LO and HEJ FOG differnt?

      // and some to absolute ratios
      scale(histos["alpha_s_abs"],1./numEvents()/scalefactor);
      scale(histos["exponential_abs"],1./numEvents()/scalefactor);
      scale(histos["virtual_exp_abs"],1./numEvents()/scalefactor);
      scale(histos["event_scale_abs"],1./numEvents()/scalefactor);

    }

    //@}

  private:
    /// @name Initialize histograms
    void inithistos() {
      histos["NJet"] = bookNLOHisto1D("NJet_incl",10,-0.5,9.5);
      histos["NJet_exc"] = bookNLOHisto1D("NJet_exc",10,-0.5,9.5);
      histos["NParticles"] = bookNLOHisto1D("NParticles_incl",15,0.5,15.5);
      histos["NParticles_exc"] = bookNLOHisto1D("NParticles_exc",15,0.5,15.5);
      histos["NNonJet"] = bookNLOHisto1D("NNon_Jet_incl",16,-0.5,15.5);
      histos["NNonJet_exc"] = bookNLOHisto1D("NNon_Jet_exc",16,-0.5,15.5);
      histos["alpha_s"] = bookNLOHisto1D("alpha_s",100,0.05,0.3);
      histos["alpha_s_abs"] = bookNLOHisto1D("alpha_s_abs",100,0.05,0.3);
      histos["exponential"] = bookNLOHisto1D("exponential",10,-5.,0.);
      histos["exponential_abs"] = bookNLOHisto1D("exponential_abs",10,-5.,0.);
      histos["virtual_exp"] = bookNLOHisto1D("virtual_exp",22,-0.5,21.5);
      histos["virtual_exp_abs"] = bookNLOHisto1D("virtual_exp_abs",22,-0.5,21.5);
      histos["event_scale"] = bookNLOHisto1D("event_scale",20,0.,1000.);
      histos["event_scale_abs"] = bookNLOHisto1D("event_scale_abs",20,0.,1000.);


      std::vector<StrHistInfo> obs;
      obs.push_back(StrHistInfo(std::string("jet12_dy_"),
                                Hist_Info(16,0.,8.)));
      obs.push_back(StrHistInfo(std::string("jet12_dymin_"),
                                Hist_Info(16,0.,8.)));
      obs.push_back(StrHistInfo(std::string("jet12_dphi_"),
                                Hist_Info(31,-3.1,3.1)));
      obs.push_back(StrHistInfo(std::string("jet12_mass_"),
                                Hist_Info(20,0.,1000.)));
      obs.push_back(StrHistInfo(std::string("xs_"),
                                Hist_Info(1,-0.5,0.5)));

      obs.push_back(StrHistInfo(std::string("logwtwt0.001_"),
                                Hist_Info(40,log(0.001),0)));
      obs.push_back(StrHistInfo(std::string("logwtwt0.00001_"),
                                Hist_Info(40,log(0.00001),0)));
      obs.push_back(StrHistInfo(std::string("logwtwt0.0000001_"),
                                Hist_Info(40,log(0.0000001),0)));
      obs.push_back(StrHistInfo(std::string("logwtwt0.000000001_"),
                                Hist_Info(40,log(0.000000001),0)));


      for (auto const & ch_emi: choices_emi) {
        for (auto const & ch_wt: choices_wt) {
          for (size_t i(0);i<obs.size();++i) {
            std::string label(obs[i].first+ch_emi+"_"+ch_wt);
            histos[label] = bookNLOHisto1D(label,
                                          obs[i].second.bins,
                                          obs[i].second.min,
                                          obs[i].second.max);
          }
        }
      }

    }

    /// @brief fill the histogram with the corresponding
    ///        choice for the emission and the weight
    void fillHistos(const Jets& jets, double const wt,
                    std::string const ch_emi, std::string const ch_wt){

      histos["xs_"+ch_emi+"_"+ch_wt]->fillAbs(0,wt,_current_event_number);
      histos["logwtwt0.001_"      +ch_emi+"_"+ch_wt]->fill(log(std::abs(wt)),wt,_current_event_number);
      histos["logwtwt0.00001_"    +ch_emi+"_"+ch_wt]->fill(log(std::abs(wt)),wt,_current_event_number);
      histos["logwtwt0.0000001_"  +ch_emi+"_"+ch_wt]->fill(log(std::abs(wt)),wt,_current_event_number);
      histos["logwtwt0.000000001_"+ch_emi+"_"+ch_wt]->fill(log(std::abs(wt)),wt,_current_event_number);

      const auto & j1 = jets[0].momentum();
      const auto & j2 = jets[1].momentum();
      double massjj((j1+j2).mass());
      double dyjj(fabs(j1.rapidity()-j2.rapidity()));
      double dymin = dyjj;
      for(size_t i=1; i<jets.size(); ++i){
        for(size_t j=0; j<i; ++j){
          const double dy = fabs(jets[i].rapidity()-jets[j].rapidity());
          if ( dy < dymin ) dymin = dy;
        }
      }

      double dphi(j1.phi()-j2.phi());
      if( dphi > PI ) dphi -= 2*PI;
      if( dphi < -PI) dphi += 2*PI;

      histos["jet12_mass_"+ch_emi+"_"+ch_wt]->fill(massjj,wt,_current_event_number);
      histos["jet12_dy_"+ch_emi+"_"+ch_wt]->fill(dyjj,wt,_current_event_number);
      histos["jet12_dymin_"+ch_emi+"_"+ch_wt]->fill(dymin,wt,_current_event_number);
      histos["jet12_dphi_"+ch_emi+"_"+ch_wt]->fill(dphi,wt,_current_event_number);
    }

    /// @name Non Jet particles
    std::vector<Particle> non_jet_part(const Jets& jets, const Particles& part) const {
      std::vector<Particle> non_jet;
      for( const auto& p: part){
        bool clustered = false;
        for( const auto& j: jets){
          if( j.containsParticle(p) ) clustered=true;
        }
        if( !clustered ) non_jet.push_back((p));
      }
      return non_jet;
    }

    /// @name Print-outs
    //@{

    void print_jet(const Jet& jet) const {
      MSG_INFO(jet << " rapidity: " << jet.rapidity());
    }

    void print_jets(const Jets& jets) const {
      for(size_t i(0); i<jets.size(); ++i){
        print_jet(jets[i]);
      }
    }

    void print_part(const Particle& prt) const {
      MSG_INFO(prt << " rapidity: " << prt.rap());
    }

    void print_parts(const Particles& prts) const {
      for(size_t i(0); i<prts.size(); ++i){
        print_part(prts[i]);
      }
    }

    void print_event(const Event& ev) const {
      // jets
      const FastJets& jetpro = applyProjection<FastJets>(ev, "Jets");
      const Jets& jets = jetpro.jetsByPt(_jpT*GeV);
      const auto& part = applyProjection<FinalState>(ev, "FS").particlesByPt();

      MSG_INFO("------------------New Event---------------------");
      MSG_INFO("wt " << ev.weight());
      MSG_INFO("all jets:");
      for( const auto & jet: jets){
        print_jet(jet);
      }
      MSG_INFO("all particles:");
      for( const auto & p: part){
        print_part(p);
      }
    }

    //@}

    /// @name HEJ virtual correction
    //@{
    double omega0(const double alpha_s, const fastjet::PseudoJet & q_j) const {
      return - alpha_s*3./M_PI*log(q_j.pt2()/LAMBDA/LAMBDA);
    }

    double virtual_exp(const Particles & pout, const ParticlePair & beam, const double alpha_s){
      std::vector<fastjet::PseudoJet> out;
      for(const auto part: pout){
        out.push_back(part.pseudojet());
        out.back().set_user_index(part.pid());
      }
      std::sort(out.begin(), out.end(),
        [](const fastjet::PseudoJet & a, const fastjet::PseudoJet & b) -> bool
          { return a.rapidity() < b.rapidity(); }
      );

      // for( auto const part: out){
      //   MSG_INFO("outcoming: " << part.rapidity() << " " << part.pt()
      //     << " " << part.user_index());
      // }

      // MSG_INFO("DeltaY " << (out.front().rapidity()-out.back().rapidity())
      //   << " -> " << strongCoupling().alphaQCD( 125.*exp(out.front().rapidity()-out.back().rapidity()) )
      //   << " given: " << alpha_s);

      fastjet::PseudoJet const & pa = beam.first.pseudojet();

      fastjet::PseudoJet q = pa - out[0];
      size_t first_idx = 0;
      size_t last_idx = out.size() - 1;
      if(out.front().user_index() == PID::HIGGS){
        q -= out[1];
        ++first_idx;
      }
      if(out.back().user_index() == PID::HIGGS){
        --last_idx;
      }
      double exponent = 0;
      for(size_t j = first_idx; j < last_idx; ++j){
        exponent += omega0(alpha_s, q)*( out[j+1].rapidity() - out[j].rapidity() );
        // MSG_INFO("virt " << j << " om " << omega0(alpha_s, q) << " dy "
        //     << (out[j+1].rapidity() - out[j].rapidity()) << " -> " <<
        //     (omega0(alpha_s, q)*( out[j+1].rapidity() - out[j].rapidity() ))
        //     << " exp=" << exponent
        //   );
        q -= out[j+1];
      }
      return exponent;
    }
    //@}


  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(HEJ_NLO_TEST);


}
