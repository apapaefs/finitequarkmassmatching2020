# BEGIN PLOT /MC_HEJ_HJET_ADD
#RatioPlotMode=deviation
#RatioPlotYMin=-10
RatioPlotMode=default
#RatioPlotYMax=1.2
#RatioPlotYMin=0.8
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/cutflow
Title=Cutflow of VBF cuts
XLabel=
YLabel=$\sigma$ [pb]
XCustomMajorTicks=0	total	1	incl.	2	2jet	3	3jet	4	VBF	5	VBF-ctrl-3jet
XMin=-0.5
XMax=5.5
RatioPlotYMin=0.90
RatioPlotYMax=1.10
#RatioPlotMode=deviation
#RatioPlotYMax=3
#RatioPlotYMin=-3
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/XS_H_within_tagjets
Title=Cross section for central Higgs production
XLabel=
YLabel=$\sigma$ [pb]
XCustomMajorTicks=0	2jet	1	h-within	2	VBF	3	h-within
XMin=-0.5
XMax=3.5
RatioPlotYMax=1.10
RatioPlotYMin=0.90
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/NJet
Title=Inclusive jet multiplicity
XLabel=$N_\text{jet}$
YLabel=$\sigma(N_\text{jet})$ [pb]
XMin=-0.5
XMax=4.5
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/HT2
Title=
XLabel=$H_T/2$ [GeV]
YLabel=d$\sigma$/d$(H_T/2)$ [pb/GeV]
LegendXPos=0.1
LegendYPos=0.5
LogX=0
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/H_pT
Title=Transverse momentum of the Higgs boson
XLabel=$p_\perp(h)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.1
LegendYPos=0.5
LogX=0
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/H_y
Title=Rapidity of the Higgs boson
XLabel=$y(h)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.5
LogY=0
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/H_jet12_pT
Title=Transverse momentum of Higgs plus leading jet system
XLabel=$p_\perp(hj_1j_2)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.5
LegendYPos=0.75
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/H_jet12_dphi
Title=Azumuthal separation of the Higgs and the leading jet system
XLabel=$\Delta\phi(h,j_1j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LegendYPos=0.75
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/jet12_dphi
Title=Azumuthal separation of the two leading jets
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LegendYPos=0.75
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/jet12_dy
Title=Rapidity separation of the two leading jets
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/jet12_njy
Title=Average number of jets
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y \times n_j\langle in \rangle$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/jet12_njy_in
Title=Average number of jets inside selected jets
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y \times n_j\langle in \rangle$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/jet12_mass
Title=Invariant mass of the two tagging jets
XLabel=$m_{jj}$ [GeV]
YLabel=d$\sigma$/d$m_{jj}$ [pb/GeV]
# END PLOT


# BEGIN PLOT /MC_HEJ_HJET_ADD/yc
Title=Central jet Veto
XLabel=$y_c$
YLabel=$\sigma(y_c)$ [pb]
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HEJ_HJET_ADD/drc
Title=Jet isolation veto
XLabel=$\Delta R_c$
YLabel=$\sigma(\Delta R_c)$ [pb]
LogY=0
ShowZero=0
# END PLOT


# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_A
\rput[lt]{0}(0.05,0.95){\bf Leading jet selection}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_B
\rput[lt]{0}(0.05,0.95){\bf Forward-backward selection}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_dijet_A
\rput[lt]{0}(0.05,0.95){\bf Leading jet selection}
\rput[lt]{0}(0.05,0.88){\bf Dijet cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_VBF_A
\rput[lt]{0}(0.05,0.95){\bf Leading jet selection}
\rput[lt]{0}(0.05,0.88){\bf VBF cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_dijet_B
\rput[lt]{0}(0.05,0.95){\bf Forward-backward selection}
\rput[lt]{0}(0.05,0.88){\bf Dijet cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_VBF_B
\rput[lt]{0}(0.05,0.95){\bf Forward-backward selection}
\rput[lt]{0}(0.05,0.88){\bf VBF cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_dijet_C
\rput[lt]{0}(0.05,0.95){\bf Leading jet selection}
\rput[lt]{0}(0.05,0.88){\bf Trijet cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_VBF_C
\rput[lt]{0}(0.05,0.95){\bf Leading jet selection}
\rput[lt]{0}(0.05,0.88){\bf VBF and Trijet cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_dijet_D
\rput[lt]{0}(0.05,0.95){\bf Forward-backward selection}
\rput[lt]{0}(0.05,0.88){\bf Trijet cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_VBF_D
\rput[lt]{0}(0.05,0.95){\bf Forward-backward selection}
\rput[lt]{0}(0.05,0.88){\bf VBF and Trijet cuts}
# END SPECIAL

# BEGIN SPECIAL /MC_HEJ_HJET_ADD/.*_incl
\rput[lt]{0}(0.05,0.95){\bf Inclusive event selection}
# END SPECIAL
