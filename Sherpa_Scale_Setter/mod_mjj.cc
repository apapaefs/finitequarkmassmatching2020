#include "PHASIC++/Scales/Scale_Setter_Base.H"

#include "PHASIC++/Process/Process_Base.H"
#include "ATOOLS/Org/Message.H"
#include "ATOOLS/Phys/Fastjet_Helpers.H"
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"

using namespace PHASIC;
using namespace ATOOLS;

namespace PHASIC {

  class Modmjj_Scale_Setter: public Scale_Setter_Base {
  private:
    const fastjet::JetAlgorithm jet_alg{fastjet::antikt_algorithm};
    const double jet_R = 0.4;
    const double jet_minpt = 30.;
    const double jet_max_rap = 4.4;
    const double minScale = 125.*125.;
    const fastjet::JetDefinition jet_def{jet_alg, jet_R};
    ATOOLS::Flavour_Vector m_f;

  public:

    Modmjj_Scale_Setter(const Scale_Setter_Arguments &args) :
      Scale_Setter_Base(args)
    {
      m_scale.resize(3); // by default three scales: fac, ren, res
                         // but you can add more if you need for COUPLINGS
      SetCouplings(); // the default value of COUPLINGS is "Alpha_QCD 1", i.e.
                      // m_scale[1] is used for running alpha_s
                      // (counting starts at zero!)
      m_f=p_proc->Flavours();
    }

    double Calculate(const std::vector<ATOOLS::Vec4D> &momenta,
		     const size_t &mode)
    {
      std::vector<fastjet::PseudoJet> input;
      for (size_t i(p_proc->NIn());i<momenta.size();++i){
        if(ToBeClustered(m_f[i],1))
          input.push_back(MakePseudoJet(m_f[i], momenta[i]));
      }

      fastjet::ClusterSequence cs(input,jet_def);
      std::vector<fastjet::PseudoJet> jets;
      {
        const std::vector<fastjet::PseudoJet> temp_jet(
          fastjet::sorted_by_pt(cs.inclusive_jets(jet_minpt)));
        for(const auto & jet: temp_jet){
          if( abs(jet.rapidity())<= jet_max_rap )
            jets.push_back(jet);
        }
      }
      {
        DEBUG_FUNC("Incoming partons");
        for(const auto & jet: input)
          DEBUG_INFO("inp (" << jet[3] << ", " << jet[0] << ", " << jet[1]
            << ", " << jet[2] <<") " << jet.rapidity() << " " << jet.pt());
      }
      {
        DEBUG_FUNC("Clustered Jets");
        for(const auto & jet: jets)
          DEBUG_INFO("jet (" << jet[3] << ", " << jet[0] << ", " << jet[1]
            << ", " << jet[2] <<") " << jet.rapidity() << " " << jet.pt());
      }

      double scale = minScale;
      if( jets.size() > 1){
        const double m12 = (jets[0]+jets[1]).m2();
        DEBUG_FUNC("Calculated m12");
        DEBUG_VAR(m12);
        if( scale < m12 ) scale = m12;
      }

      m_scale[stp::fac] = scale;
      m_scale[stp::ren] = scale;
      m_scale[stp::res] = scale;

      // Switch on debugging output for this class with:
      // Sherpa "OUTPUT=2[Custom_Scale_Setter|15]"
      DEBUG_VAR(scale);

      return m_scale[stp::fac];
    }

  };

}

// Some plugin magic to make it available for SCALES=mod_mjj
DECLARE_GETTER(Modmjj_Scale_Setter,"mod_mjj",
	       Scale_Setter_Base,Scale_Setter_Arguments);

Scale_Setter_Base *ATOOLS::Getter
<Scale_Setter_Base,Scale_Setter_Arguments,Modmjj_Scale_Setter>::
operator()(const Scale_Setter_Arguments &args) const
{
  return new Modmjj_Scale_Setter(args);
}

void ATOOLS::Getter<Scale_Setter_Base,Scale_Setter_Arguments,
		    Modmjj_Scale_Setter>::
PrintInfo(std::ostream &str,const size_t width) const
{
  str<<"Modified m12 scale setter";
}
